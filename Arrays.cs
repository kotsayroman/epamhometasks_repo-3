using System;

namespace EpamHomeTasks
{
    class Array
    {
        //PRIVATE FIELDS
        private int[] _vector;
        
        private int _minIndex;
        private int _maxIndex;
        
        //CONSTRUCTOR
        public Array(int minIndex, int maxIndex)
        {
            this._minIndex = minIndex;
            this._maxIndex = maxIndex;
            this._vector = new int[this._maxIndex - this._minIndex + 1];
        }
        
        //PUBLIC PROPERTIES
        public int MinIndex
        {
            get
            {
                return this._minIndex;
            }
        }
        
        public int MaxIndex
        {
            get
            {
                return this._maxIndex;
            }
        }
        
        public int Length
        {
            get
            {
                return this._vector.Length;
            }
        }
        
        public int this [int index]
        {
            get
            {
                if(index >= this._minIndex && index <= this._maxIndex)
                {
                    return this._vector[index - this._minIndex];
                }
                else
                {
                    throw new IndexOutOfRangeException();
                }
            }
            set
            {
                if(index >= this._minIndex && index <= this._maxIndex)
                {
                    this._vector[index - this._minIndex] = value;
                }
                else
                {
                    throw new IndexOutOfRangeException();
                }
            }
        }
        
        //PUBLIC METHODS
        public void Fill(int minValue, int maxValue)
        {
            Random rand = new Random();
            for(int i = this.MinIndex; i <= this.MaxIndex; i++)
            {
                this[i] = rand.Next(minValue, maxValue + 1);
            }
        }
        
        public override string ToString()
        {
            string result = "";
            
            for(int i = this.MinIndex; i <= this.MaxIndex; i++)
            {
                result += string.Format("{0} ", this[i]);
            }
            
            return result + "\n";
        }
        
        public static Array Add(Array a1, Array a2)
        {
            if((a1.MinIndex == a2.MinIndex) && (a1.MaxIndex == a2.MaxIndex))
            {
                Array result = new Array(a1.MinIndex, a1.MaxIndex);
                for(int i = result.MinIndex; i <= result.MaxIndex; i++)
                {
                    result[i] = a1[i] + a2[i];
                }
                return result;
            }
            else
            {
                throw new Exception("Index intervals of the terms must be equals");
            }
        }
        
        public static Array Substract(Array a1, Array a2)
        {
            if((a1.MinIndex == a2.MinIndex) && (a1.MaxIndex == a2.MaxIndex))
            {
                Array result = new Array(a1.MinIndex, a1.MaxIndex);
                for(int i = result.MinIndex; i <= result.MaxIndex; i++)
                {
                    result[i] = a1[i] - a2[i];
                }
                return result;
            }
            else
            {
                throw new Exception("Index intervals of the terms must be equals");
            }
        }
        
        public static Array Multiplication(Array a1, int scalar)
        {
            Array result = new Array(a1.MinIndex, a1.MaxIndex);
            
            for(int i = result.MinIndex; i <= result.MaxIndex; i++)
            {
                result[i] = a1[i] * scalar;
            }
            
            return result;
        }
        
        public override bool Equals(object o)
        {
            if(o is Array)
            {
                return this == (Array)o;
            }
            else
            {
                return false;
            }
        }
        
        public override int GetHashCode()
        {
            return this.ToString().GetHashCode();
        }
        
        //OVERLOADED OPERATORS
        public static Array operator +(Array a1, Array a2)
        {
            return Array.Add(a1, a2);
        }
        
        public static Array operator -(Array a1, Array a2)
        {
            return Array.Substract(a1, a2);
        }
        
        public static Array operator *(Array a1, int scalar)
        {
            return Array.Multiplication(a1, scalar);
        }
        
        public static bool operator ==(Array a1, Array a2)
        {
            if(a1.MinIndex == a2.MinIndex && a1.MaxIndex == a2.MaxIndex)
            {
                bool result = true;
                for(int i = a1.MinIndex; i <= a1.MaxIndex; i++)
                {
                    if(a1[i] != a2[i])
                    {
                        result = false;
                        break;
                    }
                }
                return result;
            }
            else
            {
                return false;
            }
        }
        
        public static bool operator !=(Array a1, Array a2)
        {
            if(a1.MinIndex == a2.MinIndex && a1.MaxIndex == a2.MaxIndex)
            {
                bool result = false;
                for(int i = a1.MinIndex; i <= a1.MaxIndex; i++)
                {
                    if(a1[i] != a2[i])
                    {
                        result = true;
                        break;
                    }
                }
                return result;
            }
            else
            {
                return true;
            }
        }
        

    }
    
    class Program
    {
        static void Main()
        {
            Array first_arr = new Array(5, 10);
            Array second_arr = new Array(5, 10);
            
            first_arr.Fill(3, 30);
            second_arr.Fill(2, 15);
            
            Console.WriteLine(first_arr);
            Console.WriteLine(second_arr);
            
            try
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("ADDITION:");
                Console.WriteLine("Array.Add(first_arr, second_arr) = {0}", Array.Add(first_arr, second_arr));           
                Console.WriteLine("first_arr + second_arr = {0}", first_arr + second_arr);
                
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("SUBSTRACTION:");
                Console.WriteLine("Array.Substract(first_arr, second_arr) = {0}", Array.Substract(first_arr, second_arr));            
                Console.WriteLine("first_arr - second_arr = {0}", first_arr - second_arr);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("MULTIPLICATION:");
            Console.WriteLine("Array.Multiplication(first_arr, 5) = {0}", Array.Multiplication(first_arr, 5));           
            Console.WriteLine("first_arr * 5 = {0}", first_arr * 5);
            
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("COMPARISON:");
            Console.WriteLine("first_arr == second_arr = {0}", first_arr == second_arr);
            Console.WriteLine("first_arr != second_arr = {0}", first_arr != second_arr);
            Console.WriteLine("first_arr == first_arr = {0}", first_arr == first_arr);
            Console.WriteLine("first_arr != first_arr = {0}", first_arr != first_arr);           
            Console.WriteLine("first_arr.Equals(second_arr) = {0}", first_arr.Equals(second_arr));
            Console.WriteLine("first_arr.Equals(first_arr) = {0}", first_arr.Equals(first_arr));
            
            Console.ForegroundColor = ConsoleColor.Gray;
            
        }
    }
}